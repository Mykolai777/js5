let userInput = prompt("Введіть число");

while(isNaN(userInput) || userInput === '' || userInput !=parseFloat ) {
  userInput = prompt("Будь ласка, введіть ціле число");
}

let num = parseInt(userInput);
let numbers = [];

for (let i = 1; i <= num; i++) {
  if (i % 5 === 0) {
    numbers.push(i);
  }
}

if (numbers.length > 0) {
  console.log("Числа, кратні 5: ", numbers);
} else {
  console.log("Sorry, no numbers");
}